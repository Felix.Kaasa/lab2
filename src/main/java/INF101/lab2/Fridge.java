package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxItems = 20;
    int currItems = 0;
    ArrayList<FridgeItem> fridgeShelf = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return currItems;
    }

    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (currItems < maxItems) {
            fridgeShelf.add(item);
            currItems++;
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        int itemIndex = fridgeShelf.indexOf(item);
        if (itemIndex == -1){
            throw new NoSuchElementException();
        }
        fridgeShelf.remove(itemIndex);
    }

    @Override
    public void emptyFridge() {
        fridgeShelf.clear();
        currItems = 0;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> removedItems = new ArrayList<FridgeItem>();

        for(FridgeItem item : fridgeShelf){
            if (item.hasExpired()){
                removedItems.add(item);
            }
        }
        for(FridgeItem item : removedItems){
            fridgeShelf.remove(item);
            currItems--;
        }
        return removedItems;
    }
}
